package com.jarsilio.android.scrambler.logging

import android.content.Context
import android.os.Build
import timber.log.Timber

const val MAX_TAG_LENGTH = 23

// I partially copied this from libcommon. // TODO think of a way to not have to have duplicate code

class Loggers {
    class LongTagTree(context: Context) : Timber.DebugTree() {
        private val packageName = context.packageName

        private fun getMessage(
            tag: String?,
            message: String,
        ): String {
            return if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.M) {
                // Tag length limitation (<23): Use truncated package name and add class name to message
                "$tag: $message"
            } else {
                // No tag length limit limitation: Use package name *and* class name
                message
            }
        }

        private fun getTag(tag: String?): String {
            var newTag: String
            if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.M) {
                // Tag length limitation (<23): Use truncated package name and add class name to message
                newTag = packageName
                if (newTag.length > MAX_TAG_LENGTH) {
                    newTag = "..." + packageName.substring(packageName.length - MAX_TAG_LENGTH + 3, packageName.length)
                }
            } else {
                // No tag length limit limitation: Use package name *and* class name
                newTag = "$packageName ($tag)"
            }

            return newTag
        }

        override fun log(
            priority: Int,
            tag: String?,
            message: String,
            t: Throwable?,
        ) {
            val newMessage = getMessage(tag, message)
            val newTag = getTag(tag)

            super.log(priority, newTag, newMessage, t)
        }
    }
}
