package com.jarsilio.android.scrambler

import android.content.ContentResolver
import android.net.Uri
import org.junit.Assert.assertTrue
import org.junit.Test
import org.mockito.Mockito
import org.mockito.Mockito.mock
import java.io.File
import java.io.FileInputStream
import java.nio.file.Files
import java.nio.file.Path

class ScramblerUnitTest {
    private fun isFileEqual(
        firstFile: Path,
        secondFile: Path,
    ): Boolean {
        if (Files.size(firstFile) != Files.size(secondFile)) {
            return false
        }
        // TODO: compare byte by byte to avoid loading the complete image in memory
        val first = Files.readAllBytes(firstFile)
        val second = Files.readAllBytes(secondFile)
        return first.contentEquals(second)
    }

    @ExperimentalUnsignedTypes
    @Test
    fun testScrambleJpeg() {
        val mockResolver: ContentResolver = mock()

        // Create a test file and InputStream
        val testFile = File(javaClass.getResource("/marvincito.jpg")!!.path)
        val testInputStream = FileInputStream(testFile)

        // Define the behavior for openInputStream
        val unscrambledFileUri = Uri.fromFile(testFile)
        Mockito.`when`(mockResolver.openInputStream(unscrambledFileUri)).thenReturn(testInputStream)

        val expectedScrambledFile = File(javaClass.getResource("/marvincito-scrambled.jpg")!!.path)
        val scrambledFile = Files.createTempFile("scrambler-", ".jpg").toFile()
        stripMetadata(
            unscrambledFileUri,
            scrambledFile,
            mockResolver
        )
        assertTrue(isFileEqual(expectedScrambledFile.toPath(), scrambledFile.toPath()))
        Files.delete(scrambledFile.toPath())
        testInputStream.close()
    }


    @ExperimentalUnsignedTypes
    @Test
    fun testScramblePng() {
        val mockResolver: ContentResolver = mock()

        // Create a test file and InputStream
        val testFile = File(javaClass.getResource("/marvincito.jpg")!!.path)
        val testInputStream = FileInputStream(testFile)

        // Define the behavior for openInputStream
        val unscrambledFileUri = Uri.fromFile(testFile)
        Mockito.`when`(mockResolver.openInputStream(unscrambledFileUri)).thenReturn(testInputStream)

        val expectedScrambledFile = File(javaClass.getResource("/marvincito-scrambled.png")!!.path)
        val scrambledFile = Files.createTempFile("scrambler-", ".png").toFile()
        stripMetadata(unscrambledFileUri, scrambledFile, mockResolver)
        assertTrue(isFileEqual(expectedScrambledFile.toPath(), scrambledFile.toPath()))
        Files.delete(scrambledFile.toPath())
    }
}
