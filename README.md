# Scrambler

An attempt at an Android image metadata stripping library.

Just playing around to see if I can manage to extract the "scrambling" code from [Scrambled Exif](https://gitlab.com/juanitobananas/scrambled-exif) in case it is useful from somebody apart from myself :)

Please feel free to use it :)

# Adding to your Android Studio Project

## As gradle dependency using jitpack.io

Follow these instructions: https://jitpack.io/#com.gitlab.juanitobananas/scrambler/

## As a git submodule

Add the library as a git submodule:

```
cd ~/git/my-cool-project
git submodule add https://gitlab.com/juanitobananas/scrambler.git
```

Note: You can (should) git commit that change to your original git repo now.

Set the git submodule as a module for gradle and Android Studio:

- Edit `settings.gradle` and add:
  ```
  include ':scrambler'
  project(':scrambler').projectDir = new File('scrambler/scrambler')
  ```

Now you just need to add it as a gradle dependency:

- In `app/build.gradle` add:
  ```
  implementation project(path: ':scrambler')
  ```
  to the dependencies section.

And that's it!

### Some basic stuff about git submodules

- To clone your project with git, you will have to use the `--recurse-submodules` option.

- You can do a git pull/checkout/whatever *inside* the scrambler directory to update it or change to a different commit.

- If you have several submodules you'll probably already know this but you can update them all to the latest upstream commit with `git submodule update --recursive --remote`

- If your app is published to F-Droid, you will have to add a `submodules=yes` to your metadata so that F-Droid knows how to build your app.

# Logging

Scrambler uses [Timber][https://github.com/JakeWharton/timber] for logging and doesn't "plant any tree" by itself, so it won't log unless you explicitly "plant a tree" yourself using Timber on your app.

This is most probably not the best way to log in a library, but I've always used Timber myself and haven't figured out a better way yet.
